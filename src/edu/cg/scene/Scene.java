package edu.cg.scene;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import edu.cg.Logger;
import edu.cg.algebra.Hit;
import edu.cg.algebra.Ops;
import edu.cg.algebra.Point;
import edu.cg.algebra.Ray;
import edu.cg.algebra.Vec;
import edu.cg.scene.lightSources.Light;
import edu.cg.scene.objects.Surface;

public class Scene {
	private String name = "scene";
	private int maxRecursionLevel = 1;
	private int antiAliasingFactor = 1; //gets the values of 1, 2 and 3
	private boolean renderRefractions = false;
	private boolean renderReflections = false;
	
	private Point camera = new Point(0, 0, 5);
	private Vec ambient = new Vec(1, 1, 1); //white
	private Vec backgroundColor = new Vec(0, 0.5, 1); //blue sky
	private List<Light> lightSources = new LinkedList<>();
	private List<Surface> surfaces = new LinkedList<>();
	
	
	//MARK: initializers
	public Scene initCamera(Point camera) {
		this.camera = camera;
		return this;
	}
	
	public Scene initAmbient(Vec ambient) {
		this.ambient = ambient;
		return this;
	}
	
	public Scene initBackgroundColor(Vec backgroundColor) {
		this.backgroundColor = backgroundColor;
		return this;
	}
	
	public Scene addLightSource(Light lightSource) {
		lightSources.add(lightSource);
		return this;
	}
	
	public Scene addSurface(Surface surface) {
		surfaces.add(surface);
		return this;
	}
	
	public Scene initMaxRecursionLevel(int maxRecursionLevel) {
		this.maxRecursionLevel = maxRecursionLevel;
		return this;
	}
	
	public Scene initAntiAliasingFactor(int antiAliasingFactor) {
		this.antiAliasingFactor = antiAliasingFactor;
		return this;
	}
	
	public Scene initName(String name) {
		this.name = name;
		return this;
	}
	
	public Scene initRenderRefarctions(boolean renderRefarctions) {
		this.renderRefractions = renderRefarctions;
		return this;
	}
	
	public Scene initRenderReflections(boolean renderReflections) {
		this.renderReflections = renderReflections;
		return this;
	}
	
	//MARK: getters
	public String getName() {
		return name;
	}
	
	public int getFactor() {
		return antiAliasingFactor;
	}
	
	public int getMaxRecursionLevel() {
		return maxRecursionLevel;
	}
	
	public boolean getRenderRefarctions() {
		return renderRefractions;
	}
	
	public boolean getRenderReflections() {
		return renderReflections;
	}
	
	@Override
	public String toString() {
		String endl = System.lineSeparator(); 
		return "Camera: " + camera + endl +
				"Ambient: " + ambient + endl +
				"Background Color: " + backgroundColor + endl +
				"Max recursion level: " + maxRecursionLevel + endl +
				"Anti aliasing factor: " + antiAliasingFactor + endl +
				"Light sources:" + endl + lightSources + endl +
				"Surfaces:" + endl + surfaces;
	}
	
	private static class IndexTransformer {
		private final int max;
		private final int deltaX;
		private final int deltaY;
		
		IndexTransformer(int width, int height) {
			max = Math.max(width, height);
			deltaX = (max - width) / 2;
			deltaY = (max - height) / 2;
		}
		
		Point transform(int x, int y) {
			double xPos = (2*(x + deltaX) - max) / ((double)max);
			double yPos = (max - 2*(y + deltaY)) / ((double)max);
			return new Point(xPos, yPos, 0);
		}
	}
	
	private transient IndexTransformer transformer = null;
	private transient ExecutorService executor = null;
	private transient Logger logger = null;
	
	private void initSomeFields(int imgWidth, int imgHeight, Logger logger) {
		this.logger = logger;
	}
	
	
	public BufferedImage render(int imgWidth, int imgHeight, Logger logger)
			throws InterruptedException, ExecutionException {
		
		initSomeFields(imgWidth, imgHeight, logger);
		
		BufferedImage img = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);
		transformer = new IndexTransformer(imgWidth, imgHeight);
		int nThreads = Runtime.getRuntime().availableProcessors();
		nThreads = nThreads < 2 ? 2 : nThreads;
		this.logger.log("Intitialize executor. Using " + nThreads + " threads to render " + name);
		executor = Executors.newFixedThreadPool(nThreads);
		
		@SuppressWarnings("unchecked")
		Future<Color>[][] futures = (Future<Color>[][])(new Future[imgHeight][imgWidth]);
		
		this.logger.log("Starting to shoot " +
			(imgHeight*imgWidth*antiAliasingFactor*antiAliasingFactor) +
			" rays over " + name);
		
		for(int y = 0; y < imgHeight; ++y)
			for(int x = 0; x < imgWidth; ++x)
				futures[y][x] = calcColor(x, y);
		
		this.logger.log("Done shooting rays.");
		this.logger.log("Wating for results...");
		
		for(int y = 0; y < imgHeight; ++y)
			for(int x = 0; x < imgWidth; ++x) {
				Color color = futures[y][x].get();
				img.setRGB(x, y, color.getRGB());
			}
		
		executor.shutdown();
		
		this.logger.log("Ray tracing of " + name + " has been completed.");
		
		executor = null;
		transformer = null;
		this.logger = null;
		
		return img;
	}
	
	private Future<Color> calcColor(int x, int y) {
		
		return executor.submit(() -> {
			Point lu = transformer.transform(x, y);
			Point rd = transformer.transform(x+1, y+1);
			
			double f = antiAliasingFactor; //var is way too long
			
			Vec color = new Vec();
			for(int i = 0; i < f;i++)
			{
				for(int j=0;j < f;j++)
				{
					Point luWei = new Point (f-j,f-i,0.0).mult(1.0/f);
					Point rdWei = new Point(j,i,0.0).mult(1.0/f);
					Point cent = Ops.add(lu.mult(luWei), rd.mult(rdWei));
					
					Ray ray = new Ray(camera, cent);
					color = color.add(calcColor(ray,0));
				}
			}

			return color.mult(1.0/(f*f)).toColor();
		});
		
	}
	
	private Vec calcColor(Ray ray, int recusionLevel) {
		if(recusionLevel >= maxRecursionLevel) return new Vec(); //if we finish our recursion
		
		Hit hit = findMinimumHit(ray);
		if(hit == null) return backgroundColor; //If we missed, get background

		Point hitPoint = ray.getHittingPoint(hit);
		Surface hitSurf = hit.getSurface();
		
		Vec color = hitSurf.Ka().mult(ambient); //set our color to the hit surface with the ambient light
		
		Vec litCol = calcColorFromLight(hitPoint,hit,ray);
		Vec refCol = calcColorFromReflections(ray,hitSurf,hit,recusionLevel);
		Vec fraCol = calcColorFromRefractions(ray,hitSurf,hit,recusionLevel);
		
		color = color.add(litCol).add(refCol).add(fraCol); //add all our components.
		//In total, we have surf + amb + light + reflec + refract		
		return color;
	}
	
	
	private Hit findMinimumHit(Ray ray)
	{
		Hit ret;
		
		//In parallel, for every surface, map to a hit via the intersect function, and filter the nulls.
		//Then, find the minimum and return it if it exists. Otherwise, return null
		ret = surfaces.parallelStream().map(x -> x.intersect(ray))
				.filter(x -> x != null).min((x,y) -> x.compareTo(y)).orElse(null);
		//Isnt functional programming fun :D?
		
		return ret;
	}
	
	
	private Vec calcColorFromLight(Point p,Hit h,Ray viewRay)
	{
		//I should use lisp for this
		
		//In parallel, for every light source, we first filter all occluded light sources
		//Then, we map each light to a Vec of its diffuse values, added to its
		// specular highlights, and then finally multiplied by the lights intensity to the object
		
		//Then, after all that, we aggrigate it down to a single Vec via .add() and, if it exists,
		//return it
		Vec colorFromLight = lightSources.parallelStream().filter(l -> !isLightRayOccluded(l, l.getLightRay(p))) //remove all occluded from stream
		.map(l -> calcLightDiffuse(h,l.getLightRay(p)) //ever l becomes (diff + spec) * intens
				.add(calcLightSpecular(h, l.getLightRay(p), viewRay))		
				.mult(l.getIntensity(p, l.getLightRay(p))))
		.reduce((x,y) -> x.add(y)).orElse(new Vec()); //stream is aggrigated down to a single vec
		
		return colorFromLight;
	}
	
	
	//Calculate the color addition from reflections, using a simple recursive calcColor
	private Vec calcColorFromReflections(Ray r, Surface s, Hit h,int recLevel)
	{
		
		Vec refDir = Ops.reflect(r.direction(), h.getNormalToSurface());
		Vec refWei = s.Ks().mult(s.reflectionIntensity());
		Vec refCol = calcColor(new Ray(r.getHittingPoint(h),refDir), recLevel +1).mult(refWei);
		return refCol; 
	}
	
	//Same as reflections, with a simple check for transparency
	private Vec calcColorFromRefractions(Ray r, Surface s, Hit h,int recLevel)
	{
		Vec fraCol = new Vec();
		if(s.isTransparent())
		{
			double n1 = s.n1(h);
			double n2 = s.n2(h);
			Vec fraDir = Ops.refract(r.direction(), h.getNormalToSurface(), n1, n2);
			Vec fraWei = s.Kt().mult(s.refractionIntensity());
			fraCol = calcColor(new Ray(r.getHittingPoint(h), fraDir), recLevel +1).mult(fraWei);
		}	
		return fraCol;
	}
	
	private boolean isLightRayOccluded(Light l, Ray r)
	{
		//In parallel, we create a stream that consists only of objects that occlude our light ray.
		//if something exists in the stream, we are occluded, and return true. Else, return false
		
		return surfaces.parallelStream().filter(s -> l.doesSurfaceOcclude(s, r)).findFirst().isPresent();
	}
	
	//Calculates the diffusion of a light ray to a surface hit
	private Vec calcLightDiffuse(Hit h, Ray r)
	{
		Vec dir = r.direction();
		Vec norm = h.getNormalToSurface();
		Vec kd = h.getSurface().Kd(r.source());
		return kd.mult(Math.max(norm.dot(dir), 0.0)); //multiply our alpha by the dotprod of the norm and the direction
	}
	
	private Vec calcLightSpecular(Hit h, Ray lightRay, Ray viewRay)
	{
		Vec dir = lightRay.direction();
		Vec norm = h.getNormalToSurface();
		Vec refl = Ops.reflect(dir.neg(), norm);
		Vec ks = h.getSurface().Ks();
		Vec v = viewRay.direction();
		int shine = h.getSurface().shininess();
		double dot = refl.dot(v.neg());
		return (dot < 0.0) ? new Vec(): ks.mult(Math.pow(dot, shine));
	}
	
}
