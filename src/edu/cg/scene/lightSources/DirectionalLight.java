package edu.cg.scene.lightSources;

import edu.cg.algebra.Point;
import edu.cg.algebra.Ray;
import edu.cg.algebra.Vec;
import edu.cg.scene.objects.Surface;

public class DirectionalLight extends Light {
	private Vec direction = new Vec(0, -1, -1);

	public DirectionalLight initDirection(Vec direction) {
		this.direction = direction;
		return this;
	}

	@Override
	public String toString() {
		String endl = System.lineSeparator();
		return "Directional Light:" + endl + super.toString() +
				"Direction: " + direction + endl;
	}

	@Override
	public DirectionalLight initIntensity(Vec intensity) {
		return (DirectionalLight)super.initIntensity(intensity);
	}

	@Override
	public boolean doesSurfaceOcclude(Surface s, Ray r) {
		if(s.intersect(r) != null) return true;
		return false;
	}

	@Override
	public Ray getLightRay(Point p) {
		return new Ray(p, direction.neg());
	}

	@Override
	public Vec getIntensity(Point p, Ray r) {
		Vec dir = direction.normalize().neg();
		Vec lit = r.direction();
		return intensity.mult(dir.dot(lit));
	}
	
	//TODO: add some methods
}
