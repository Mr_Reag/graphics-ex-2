package edu.cg.scene.lightSources;

import edu.cg.algebra.Hit;
import edu.cg.algebra.Point;
import edu.cg.algebra.Ray;
import edu.cg.algebra.Vec;
import edu.cg.scene.objects.Surface;

public class PointLight extends Light {
	protected Point position;
	
	//Decay factors:
	protected double kq = 0.01;
	protected double kl = 0.1;
	protected double kc = 1;
	
	protected String description() {
		String endl = System.lineSeparator();
		return "Intensity: " + intensity + endl +
				"Position: " + position + endl +
				"Decay factors: kq = " + kq + ", kl = " + kl + ", kc = " + kc + endl;
	}
	
	@Override
	public String toString() {
		String endl = System.lineSeparator();
		return "Point Light:" + endl + description();
	}
	
	@Override
	public PointLight initIntensity(Vec intensity) {
		return (PointLight)super.initIntensity(intensity);
	}
	
	public PointLight initPosition(Point position) {
		this.position = position;
		return this;
	}
	
	public PointLight initDecayFactors(double kq, double kl, double kc) {
		this.kq = kq;
		this.kl = kl;
		this.kc = kc;
		return this;
	}

	@Override
	public boolean doesSurfaceOcclude(Surface s, Ray r) {
		
		Hit hit = s.intersect(r);
		if(hit == null) return false;
		
		Point p = r.source();
		Point hitPoint = r.getHittingPoint(hit);
		
		return p.distSqr(position) > p.distSqr(hitPoint); //if the object is closer than the hit, its occluded
	}

	@Override
	public Ray getLightRay(Point p) {
		return new Ray(p, position);
	}

	@Override
	public Vec getIntensity(Point p, Ray r) {
		double dist = p.dist(position);
		double decay = kc +(kl + kq*dist)*dist;
		return intensity.mult(1.0/decay);
	}

	//TODO: add some methods
}
