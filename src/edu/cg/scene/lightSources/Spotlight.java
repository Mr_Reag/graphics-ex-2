package edu.cg.scene.lightSources;

import edu.cg.algebra.Point;
import edu.cg.algebra.Ray;
import edu.cg.algebra.Vec;
import edu.cg.scene.objects.Surface;

public class Spotlight extends PointLight {
	private Vec direction;
	private double angle = 0.866; //cosine value ~ 30 degrees
	
	public Spotlight initDirection(Vec direction) {
		this.direction = direction;
		return this;
	}
	
	public Spotlight initAngle(double angle) {
		this.angle = angle;
		return this;
	}
	
	@Override
	public String toString() {
		String endl = System.lineSeparator();
		return "Spotlight: " + endl +
				description() + 
				"Direction: " + direction + endl +
				"Angle: " + angle + endl;
	}
	
	@Override
	public Spotlight initPosition(Point position) {
		return (Spotlight)super.initPosition(position);
	}
	
	@Override
	public Spotlight initIntensity(Vec intensity) {
		return (Spotlight)super.initIntensity(intensity);
	}
	
	@Override
	public Spotlight initDecayFactors(double q, double l, double c) {
		return (Spotlight)super.initDecayFactors(q, l, c);
	}
	
	
	@Override
	public boolean doesSurfaceOcclude(Surface s, Ray r)
	{
		//Is the object inside the spotlights area?
		if(r.direction().neg().dot(direction.normalize()) < angle) return true;
		return super.doesSurfaceOcclude(s, r);
		
	}
	
	@Override
	public Vec getIntensity(Point p, Ray r)
	{
		Vec dir = direction.normalize().neg();
		Vec raydir = r.direction();
		return super.getIntensity(p, r).mult(dir.dot(raydir)); //scale by spotlight direction
	}
	
}
