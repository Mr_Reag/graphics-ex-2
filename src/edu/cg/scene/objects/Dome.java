package edu.cg.scene.objects;

import edu.cg.algebra.Hit;
import edu.cg.algebra.Ops;
import edu.cg.algebra.Point;
import edu.cg.algebra.Ray;
import edu.cg.algebra.Vec;

public class Dome extends Shape {
	private Sphere sphere;
	private Plain plain;
	
	public Dome() {
		sphere = new Sphere().initCenter(new Point(0, -0.5, -6));
		plain = new Plain(new Vec(-1, 0, -1), new Point(0, -0.5, -6));
	}
	
	@Override
	public String toString() {
		String endl = System.lineSeparator();
		return "Dome:" + endl + 
				sphere + plain + endl;
	}
	
	@Override
	public Hit intersect(Ray ray) {
		
		Hit hit = sphere.intersect(ray);
		if(hit == null) return null;
		return (hit.isWithinTheSurface()) ? insideHit(ray,hit) : outsideHit(ray,hit);

	}
	
	private Hit outsideHit(Ray r, Hit h)
	{
		//If we hit the sphere
		Point hitPoint = r.getHittingPoint(h);
		if(plain.pointToDist(hitPoint) > 0.0) return h;
		
		//Otherwise, we check the plain
		Hit plainHit = plain.intersect(r);
		if(plainHit == null) return null;
		
		hitPoint = r.getHittingPoint(plainHit);
		if(sphere.distToSphere(hitPoint) < 0.0) return plainHit;
		return null;
	}
	
	private Hit insideHit(Ray r, Hit h)
	{
		Point hitPoint = r.getHittingPoint(h);
		//if we're not too close to the source of the ray
		if(plain.pointToDist(r.source()) > Ops.epsilon)
		{
			//if we hit the sphere
			if(plain.pointToDist(hitPoint) > 0) return h;
			
			//Otherwise, we check the plain
			h = plain.intersect(r);
			if(h == null) return null;
			
			return h.setWithin();
		}
		// otherwise, we're not actually within the dome
		if(plain.pointToDist(hitPoint) > 0.0) return plain.intersect(r);
		
		return null;
	}
}
