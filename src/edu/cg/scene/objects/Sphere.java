package edu.cg.scene.objects;

import edu.cg.algebra.Hit;
import edu.cg.algebra.Ops;
import edu.cg.algebra.Point;
import edu.cg.algebra.Ray;
import edu.cg.algebra.Vec;

public class Sphere extends Shape {
	private Point center;
	private double radius;
	
	public Sphere(Point center, double radius) {
		this.center = center;
		this.radius = radius;
	}
	
	public Sphere() {
		this(new Point(0, -0.5, -6), 0.5);
	}
	
	@Override
	public String toString() {
		String endl = System.lineSeparator();
		return "Sphere:" + endl + 
				"Center: " + center + endl +
				"Radius: " + radius + endl;
	}
	
	public Sphere initCenter(Point center) {
		this.center = center;
		return this;
	}
	
	public Sphere initRadius(double radius) {
		this.radius = radius;
		return this;
	}
	
	@Override
	public Hit intersect(Ray ray) {

		//We will use the discriminant of the quadratic to speed up calculation
		//Get dot prod the source with the center of the object. *2 to get b for ax2+bx+c
		double b = ray.direction().mult(2.0).dot(ray.source().sub(center));
		double c = distToSphere(ray.source()); //sqrd distance to sphere, from ray source
		
		double dis = Math.sqrt(b*b - 4.0 * c); //discriminant
		
		//If there are no roots, aka miss
		if(Double.isNaN(dis)) return null;
		double root1 = (-b-dis) / 2.0; //closer root
		double root2 = (-b+dis) / 2.0; //farther root
		
		//If the sphere is inline with the ray, but behind us
		if(root2 < Ops.epsilon) return null;
		
		//If root 2 is ahead, but root 1 is behind (inside)
		boolean inside = false;
		if(root1 < Ops.epsilon) {root1 = root2; inside = true;}
		
		double minR = Math.min(root1, root2); //get min root
		if(!Ops.isFiniteAndNonNeg(minR)) return null; //infinite distance to hit, aka miss
		
		Vec normal = normal(ray.add(minR)); //get the normal to the intersection
		if(inside) normal = normal.neg(); //we hit the inside, so get the opposite normal
		
		//Return our hit!
		
		//System.out.println("Valid sphere hit found");
		
		return new Hit(minR,normal).setIsWithin(inside);
	}
	
	//
	public double distToSphere(Point p)
	{
		return p.distSqr(center) - radius*radius;
	}
	
	public Vec normal(Point p)
	{
		return p.sub(center).normalize();
	}
}
