package edu.cg.scene.objects;

import edu.cg.algebra.Hit;
import edu.cg.algebra.Point;
import edu.cg.algebra.Ray;
import edu.cg.algebra.Vec;

public class Triangle extends Shape {
	private Point p1, p2, p3;
	private transient Plain plain = null;
	
	public Triangle() {
		p1 = p2 = p3 = null;
	}
	
	public Triangle(Point p1, Point p2, Point p3) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		setupPlain();
	}
	
	@Override
	public String toString() {
		String endl = System.lineSeparator();
		return "Triangle:" + endl +
				"p1: " + p1 + endl + 
				"p2: " + p2 + endl +
				"p3: " + p3 + endl;
	}

	//we use a plane -> inner triangle method
	@Override
	public Hit intersect(Ray ray) {
		Hit hit = plain.intersect(ray);
		if(hit != null && onTriangle(ray, hit))
		{
			return hit;
		}
		return null;
	}

	//if we're on the plane, lets figure out if its in the triangle
	private boolean onTriangle(Ray ray, Hit hit) {
		
		Triangle[] triangles = {
			new Triangle(ray.source(),p1,p2),
			new Triangle(ray.source(),p2,p3),
			new Triangle(ray.source(),p3,p1)
		};

		int fcount = 0;
		for(int i=0 ; i < 3; i++)
		{
			if(triangles[i].plain.normal().dot(ray.direction()) <= 0.0)
			{
				fcount++;
			}
		}
		
		if(fcount == 0 || fcount == 3) return true;			
		return false;
	}

	//Build a plain for our triangle, to hold the normal
	private void setupPlain()
	{
		if(plain == null) {
			Vec edge1 = p2.sub(p1);
			Vec edge2 = p3.sub(p1);
			Vec normal = edge1.cross(edge2).normalize();
			plain = new Plain(normal, p1);
		}

	}
}
